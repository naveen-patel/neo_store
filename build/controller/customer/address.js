"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// for add address details
var address_1 = __importDefault(require("../../Models/customer/address"));
/**
*
* @param customer_id customer id from token
* @param address address of customer
* @param pincode pincode of area
* @param city city
* @param state state
* @param country country
*
*/
var address = function (req, res) {
    var newData = new address_1.default({
        customer_id: req.body._id,
        address: req.body.address,
        pincode: req.body.pincode,
        city: req.body.city,
        state: req.body.state,
        country: req.body.country,
    });
    newData.save()
        .then(function (result) {
        res.status(200).json({ status_code: 200, success: true, message: "user address successfully added", product: result });
    })
        .catch(function (err) {
        res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err });
    });
};
exports.default = address;
