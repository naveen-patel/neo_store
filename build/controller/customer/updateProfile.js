"use strict";
// for user profile details update
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var customer_1 = __importDefault(require("../../Models/customer/customer"));
/**
* @param first_name
* @param last_name
* @param gender
* @param dob
* @param phone
* @param email
* @param profile_image
* @returns update profile of user
*/
var updatePro = function (req, res) {
    customer_1.default.findByIdAndUpdate(req.body._id, {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        gender: req.body.gender,
        dob: req.body.dob,
        phone: req.body.phone,
        email: req.body.email,
        profile_image: req.file.filename
    })
        .then(function (result) {
        customer_1.default.find({ _id: req.body._id })
            .then(function (result) {
            var data = result;
            res.status(200).json({ status_code: 200, success: true, message: "user profile successfully update", details: data[0] });
        });
    })
        .catch(function (err) {
        res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err });
    });
};
exports.default = updatePro;
