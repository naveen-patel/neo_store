"use strict";
// for user address  details update
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var address_1 = __importDefault(require("../../Models/customer/address"));
/**
* @param _id id of user from token

* @returns updates address of user
*/
var delivery = function (req, res) {
    address_1.default.findByIdAndUpdate(req.params.address_id, {
        flag: "true"
    })
        .then(function (result) {
        address_1.default.find({ _id: req.params.address_id })
            .then(function (result) {
            res.status(200).json({ status_code: 200, success: true, message: "Address successfully updated ", details: result });
        });
    })
        .catch(function (err) {
        res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err });
    });
};
exports.default = delivery;
