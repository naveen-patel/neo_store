"use strict";
//for registration new user
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var customer_1 = __importDefault(require("../../Models/customer/customer"));
var joi_1 = __importDefault(require("joi"));
var bcrypt_1 = __importDefault(require("bcrypt"));
var sendmail_1 = __importDefault(require("sendmail"));
var newCustomer = function (req, res) {
    var emailSender = sendmail_1.default({
        silent: false
    });
    var sendEmail = function (options) {
        return new Promise(function (resolve, reject) {
            emailSender(options, function (err, reply) {
                // if error happened or returned code is now started with 2**
                if (err || !reply.startsWith("2")) {
                    reject(err);
                }
                else {
                    resolve(true);
                }
            });
        });
    };
    //for empty field
    if (req.body === undefined) {
        return res
            .status(404)
            .json({ success: false, message: "Something went wrong" });
    }
    // for validation
    var schema = joi_1.default.object().keys({
        first_name: joi_1.default.string().required(),
        last_name: joi_1.default.string().required(),
        email: joi_1.default.string()
            .email({ minDomainAtoms: 2 }).regex(/^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i)
            .required(),
        password: joi_1.default.string()
            .regex(/^(?=.*[a-zA-Z])(?=.*[0-9])/)
            .required(),
        conpassword: joi_1.default.any()
            .equal(joi_1.default.ref("password"))
            .required(),
        gender: joi_1.default.string().required(),
        phone: joi_1.default.number()
            .integer()
            .required()
    });
    joi_1.default.validate(req.body, schema, function (err, value) {
        if (err) {
            return res
                .status(400)
                .json({
                err: err,
                code: 400,
                message: "joi validation error",
                error_message: err
            });
        }
        else {
            // for password encryption
            var saltRounds = 10;
            var myPlaintextPassword = req.body.password;
            var salt = bcrypt_1.default.genSaltSync(saltRounds);
            var passwordHash = bcrypt_1.default.hashSync(myPlaintextPassword, salt);
            var output_1 = "<div style=\"background-color:skyblue;\"'>\n     \n<h3>Hi " + (req.body.first_name + req.body.last_name) + " your details are mentioned below:</h3>\n<ul>  \n  <li>Name:</li>\n  <li><a href=\"www.google.com\">Visit here</a></li>\n</ul>\n</div>";
            /**
            * @param email
            * @param password password must be minimum 4 characters long and 8 maximum characters long and should contains at least one capital letter, one small letter and one digit
            * @param cnfpassword confirm password
            * @param gender numeric in form of 0 or 1, 1 for male and 0 for female
            * @param phone
            * @param first_name
            * @param last_name
            */
            var newData = new customer_1.default({
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                email: req.body.email,
                password: passwordHash,
                gender: req.body.gender,
                phone: req.body.phone
            });
            newData
                .save()
                .then(function (result) {
                sendEmail({
                    from: 'naveen.patel@neosofttech.com',
                    to: req.body.email,
                    subject: 'User login details',
                    html: output_1,
                });
                res.status(200).json({ status_code: 200, success: true, message: "Customer Registration Successfully", customer: result });
            })
                .catch(function (err) {
                res
                    .status(404)
                    .json({
                    success: false,
                    status_code: 404,
                    message: "Something went wrong",
                    error_message: err
                });
            });
        }
    });
};
exports.default = newCustomer;
