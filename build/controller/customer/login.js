"use strict";
// for user login
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var customer_1 = __importDefault(require("../../Models/customer/customer"));
var joi_1 = __importDefault(require("joi"));
var jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
var bcrypt_1 = __importDefault(require("bcrypt"));
var address_1 = __importDefault(require("../../Models/customer/address"));
var cart_1 = __importDefault(require("../../Models/product/cart"));
var customerLogin = function (req, res) {
    var id;
    var schema = joi_1.default.object().keys({
        email: joi_1.default.string().email({ minDomainAtoms: 2 }).regex(/^[a-zA-Z]+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$/).required(),
        password: joi_1.default.string().regex(/^[a-zA-Z0-9]{3,30}$/).required()
    });
    joi_1.default.validate(req.body, schema, function (err, value) {
        if (err) {
            return res
                .status(400)
                .json({
                err: err,
                status_code: 400,
                message: "joi validation error",
                error_message: err
            });
        }
        else {
            customer_1.default.findOne({ email: req.body.email })
                .then(function (result) {
                if (result == null) {
                    return res.status(404).json({ status_code: 404, success: false, message: "Mail not found", error_message: err });
                }
                else {
                    var _a = result.toObject(), password = _a.password, user_1 = __rest(_a, ["password"]); //{expiresIn:'30s'},
                    if (bcrypt_1.default.compareSync(req.body.password, password)) {
                        jsonwebtoken_1.default.sign({ email: result.email, id: result._id }, 'secretkey', function (err, token) {
                            address_1.default.find({ customer_id: user_1._id })
                                .then(function (results) {
                                cart_1.default.find({ customer_id: user_1._id })
                                    /**
                                    * @param email
                                    * @param password
                                    * @returns details of user and cart count
                                    */
                                    .then(function (result) {
                                    // OrderModel.find({ customer_id: user._id,flag:true})
                                    // .then(shipped=>{
                                    //     OrderModel.find({ customer_id: user._id,flag:false})
                                    //     .then(placed=>{
                                    res.status(200).json({ status_code: 200, success: true, message: "Successfully login ", token: token, data: user_1, cart_count: result.length, Address: results });
                                });
                                //     })
                                // })
                            });
                        });
                    }
                    else {
                        res.status(404).json({ status_code: 404, success: false, message: "Password not match", error_message: err });
                    }
                }
            })
                .catch(function (err) {
                res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err });
            });
        }
    });
};
exports.default = customerLogin;
