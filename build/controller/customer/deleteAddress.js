"use strict";
// for delete user address details
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var address_1 = __importDefault(require("../../Models/customer/address"));
/**
* @param _id user id from token whose address to be deleted
* @param address_id address id
* @returns delete selected address of user
*/
var deleteaddress = function (req, res) {
    address_1.default.deleteOne({ _id: req.params.address_id, customer_id: req.body._id })
        .then(function (result) {
        res.status(200).json({ status_code: 200, success: true, message: "Address successfully deleted ", details: result });
    })
        .catch(function (err) {
        res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err });
    });
};
exports.default = deleteaddress;
