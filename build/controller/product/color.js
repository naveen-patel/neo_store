"use strict";
// for inserting data in color collections
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var color_1 = __importDefault(require("../../Models/product/color"));
var newColor = function (req, res) {
    /**
    * @param color_name
    * @param color_code address id
    */
    var newData = new color_1.default(req.body);
    newData
        .save()
        .then(function (result) {
        res.status(200).json({ status_code: 200, success: true, message: "Product color details successfully inserted", customer: result });
    })
        .catch(function (err) {
        res
            .status(404)
            .json({
            status_code: 404,
            success: false,
            message: "Something went wrong",
            error_message: err
        });
    });
};
exports.default = newColor;
