"use strict";
//for product by id (return one product's details with color and category )
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var product_1 = __importDefault(require("../../Models/product/product"));
var productById = function (req, res) {
    // const { email, prod_color="red", prod_type } = req.body;
    product_1.default.find({ _id: req.body._id })
        .populate('color_id')
        .populate('category_id')
        .then(function (result) {
        res.status(200).json({ status_code: 200, success: true, message: "product detail by id", product: result });
    })
        .catch(function (err) {
        res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err });
    });
};
exports.default = productById;
