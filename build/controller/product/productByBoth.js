"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// for find product by both color and category
var product_1 = __importDefault(require("../../Models/product/product"));
/**
* @param category_id to sort products according to category id
* @param color_id to sort products according to color id
* @param sortBy to sort product according to rating or price(sortBy=product_rating)
* @param desc shows the way of sorting product (sortBy=product_price&desc=false) will sort product according to price in descending order
* @param text for searching product
*/
var productByBoth = function (req, res) {
    var _a;
    var a = {};
    var _b = req.query, _c = _b.sort, sort = _c === void 0 ? '{}' : _c, limit = _b.limit, page = _b.page, _d = _b.text, text = _d === void 0 ? '' : _d, sortBy = _b.sortBy, desc = _b.desc, others = __rest(_b, ["sort", "limit", "page", "text", "sortBy", "desc"]); // destructering
    if (text.trim()) {
        a.product_name = { $regex: "^" + text, $options: 'i' };
    }
    Object.entries(others).forEach(function (item) { if (item[1])
        a[item[0]] = item[1]; });
    product_1.default.find(a).sort(sortBy ? (_a = {}, _a[sortBy] = desc == "true" ? -1 : 1, _a) : {})
        .limit(parseInt(req.query.limit)).skip(parseInt(req.query.limit) * (parseInt(req.query.page) - 1))
        .populate('category_id')
        .populate('color_id')
        .then(function (result) {
        var _a;
        product_1.default.find(a).sort(sortBy ? (_a = {}, _a[sortBy] = desc == "true" ? -1 : 1, _a) : {})
            .then(function (data) {
            res.status(200).json({ success: 'true', status: 200, product: result, count: result.length, totalcount: data.length });
        });
    })
        .catch(function (err) {
        res.status(404).json({ success: 'false', status: 404, err: err.details });
    });
};
exports.default = productByBoth;
