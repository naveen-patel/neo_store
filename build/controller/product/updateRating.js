"use strict";
// for user address  details update
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var product_1 = __importDefault(require("../../Models/product/product"));
/**
* @param _id id of product whose rating needs to be updated
* @param rating new rating given by user
* @returns new average rating
*/
var updaterating = function (req, res) {
    if (req.body.product_rating <= 5 && req.body.product_rating >= 0) {
        product_1.default.find({ _id: req.body.product_id })
            .then(function (result) {
            var total = (parseFloat(result[0].totalrating) + parseFloat(req.body.product_rating));
            var count = (parseFloat(result[0].product_ratingCount) + 1);
            var average = total / count;
            product_1.default.findByIdAndUpdate(req.body.product_id, {
                product_rating: average,
                product_ratingCount: count,
                totalrating: total
            })
                .then(function (result) {
                product_1.default.find({ _id: req.body.product_id })
                    .then(function (result) {
                    res.status(200).json({ status_code: 200, success: true, message: "Product rating successfully updated ", details: result });
                });
            });
        })
            .catch(function (err) {
            res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err });
        });
    }
    else {
        res.status(404).json({ status_code: 404, success: false, message: "rating not correct (0<=5)" });
    }
};
exports.default = updaterating;
