"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var product_1 = __importDefault(require("../../Models/product/product"));
/**
* @param _id user id from token whose address to be deleted
* @returns all address of customers
*/
var topRated = function (req, res) {
    product_1.default.aggregate([
        ([{
                $lookup: {
                    from: "colors",
                    localField: "color_id",
                    foreignField: "color_id",
                    as: "color_id"
                }
            },
            {
                $sort: { product_rating: -1 }
            },
            {
                $group: {
                    _id: "$category_id",
                    // products:{$push:"$$ROOT"}
                    products: { $first: "$$ROOT" }
                }
            }
        ])
    ])
        .then(function (result) {
        res.status(200).json({ status_code: 200, success: true, message: "Top rated product", product: result });
    })
        .catch(function (err) {
        res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err });
    });
};
exports.default = topRated;
