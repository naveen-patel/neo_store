"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// for all color and color code (return color with code)
var category_1 = __importDefault(require("../../Models/product/category"));
var allCategory = function (req, res) {
    category_1.default.find()
        .then(function (result) {
        res.status(200).json({ status_code: 200, success: true, message: "All category of product", product: result });
    })
        .catch(function (err) {
        res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err });
    });
};
exports.default = allCategory;
