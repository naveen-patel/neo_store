"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// for inserting product details
var product_1 = __importDefault(require("../../Models/product/product"));
/**
* @param category_id product category id
* @param color_id product color id
* @param product_name
* @param product_color
* @param product_desc
* @param product_producer
* @param product_stock
* @param product_type
* @param product_image
* @param product_dimensions
* @param product_material
* @param product_cost
* @param product_rating

*/
var newProduct = function (req, res) {
    var image = [];
    for (var k = 0; k < req.files.length; k++) {
        image.push(req.files[k].filename);
    }
    var newData = new product_1.default({
        category_id: req.body.category_id,
        color_id: req.body.color_id,
        product_name: req.body.product_name,
        product_color: req.body.product_color,
        product_desc: req.body.product_desc,
        product_producer: req.body.product_producer,
        product_stock: req.body.product_stock,
        product_type: req.body.product_type,
        product_image: image,
        product_dimensions: req.body.product_dimensions,
        product_material: req.body.product_material,
        product_cost: req.body.product_cost,
        product_rating: req.body.product_rating
    });
    newData
        .save()
        .then(function (result) {
        res.status(200).json({ status_code: 200, success: true, message: "Product details successfully inserted ", customer: result });
    })
        .catch(function (err) {
        res
            .status(404)
            .json({
            status_code: 404,
            success: false,
            message: "Something went wrong",
            error_message: err
        });
    });
};
exports.default = newProduct;
