"use strict";
//for order details of 
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var order_1 = __importDefault(require("../../Models/product/order"));
/**
* @param _id user id from token whose address to be deleted
* @returns group of cart product by orderId
*/
var order = function (req, res) {
    order_1.default.aggregate([
        ([
            { $match: { customer_id: req.body._id } },
            {
                $lookup: {
                    from: "products",
                    localField: "product_id",
                    foreignField: "product_id",
                    as: "order"
                }
            },
            {
                $group: {
                    _id: "$order_id",
                    products: { $push: "$$ROOT" }
                    //products: { $first: "$$ROOT" }
                }
            }
        ])
    ])
        .then(function (result) {
        res.status(200).json({ status_code: 200, success: true, message: "order details of customer", product: result });
    })
        .catch(function (err) {
        res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err });
    });
};
exports.default = order;
