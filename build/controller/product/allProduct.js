"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// for all product (return all product)
var product_1 = __importDefault(require("../../Models/product/product"));
/**
* @returns all product details
*/
var allProduct = function (req, res) {
    product_1.default.find().select("product_name")
        .populate('category_id')
        .then(function (result) {
        res.status(200).json({ status_code: 200, success: true, message: "all product", product: result });
    })
        .catch(function (err) {
        res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err });
    });
};
exports.default = allProduct;
