"use strict";
// for delete user address details
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var cart_1 = __importDefault(require("../../Models/product/cart"));
/**
* @param _id user id from token whose address to be deleted
* @param cart_Id cart id
* @returns delete selected product form cart
*/
var deletecart = function (req, res) {
    cart_1.default.deleteOne({ product_id: req.params.product_id, customer_id: req.body._id })
        .then(function (result) {
        res.status(200).json({ status_code: 200, success: true, message: "product successfully deleted ", details: result });
    })
        .catch(function (err) {
        res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err });
    });
};
exports.default = deletecart;
