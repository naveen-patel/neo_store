"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// for all product (return all product)
var order_1 = __importDefault(require("../../Models/product/order"));
/**
* @returns all product details
*/
var shippedcount = function (req, res) {
    order_1.default.find({ customer_id: req.body._id, flag: true })
        .then(function (shipped) {
        order_1.default.find({ customer_id: req.body._id, flag: false })
            .then(function (placed) {
            res.status(200).json({ status_code: 200, success: true, message: "Successfully login ", placed: placed.length, shipped: shipped.length });
        });
    })
        .catch(function (err) {
        res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err });
    });
};
exports.default = shippedcount;
