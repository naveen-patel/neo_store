"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// for all color and color code (return color with code)
var color_1 = __importDefault(require("../../Models/product/color"));
var allColor = function (req, res) {
    color_1.default.find()
        .then(function (result) {
        res.status(200).json({ status_code: 200, success: true, message: "All color and color code of product", product: result });
    })
        .catch(function (err) {
        res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err });
    });
};
exports.default = allColor;
