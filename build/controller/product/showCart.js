"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// for cart details (return all product from cart)
var cart_1 = __importDefault(require("../../Models/product/cart"));
/**
* @param _id user id from token whose address to be deleted
* @returns all product of cart
*/
var cartDetails = function (req, res) {
    cart_1.default.find({ customer_id: req.body._id })
        .populate('product_id')
        .then(function (result) {
        res.status(200).json({
            status_code: 200, success: true, message: "cart details", data: result
        });
    })
        .catch(function (err) {
        res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err });
    });
};
exports.default = cartDetails;
