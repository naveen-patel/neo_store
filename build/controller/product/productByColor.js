"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// Find product by Color
var product_1 = __importDefault(require("../../Models/product/product"));
var productByColor = function (req, res) {
    product_1.default.find({ color_id: req.body.col_id, })
        .populate('category_id')
        .populate('color_id')
        .then(function (result) {
        res.status(200).json({ status_code: 200, success: true, message: "success", product: result });
    })
        .catch(function (err) {
        res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err });
    });
};
exports.default = productByColor;
