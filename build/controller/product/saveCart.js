"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// for inserting data in cart collections
var cart_1 = __importDefault(require("../../Models/product/cart"));
var order_1 = __importDefault(require("../../Models/product/order"));
var product_1 = __importDefault(require("../../Models/product/product"));
var address_1 = __importDefault(require("../../Models/customer/address"));
var newCart = function (req, res) {
    try {
        var orderId_1 = "order_id" + Date.now();
        // for checkout (order details save )
        var cartdetails_1 = req.body;
        var bodylength_1 = cartdetails_1.length - 2;
        console.log('cart details ', cartdetails_1);
        console.log('true cart details', cartdetails_1[bodylength_1].flag);
        if (cartdetails_1[bodylength_1].flag == "true") {
            var totalcost_1 = 0;
            var _loop_1 = function (k) {
                // for unique orderId
                product_1.default.find({ _id: cartdetails_1[k]._id })
                    .then(function (result) {
                    address_1.default.find({ _id: cartdetails_1[bodylength_1 + 1].address_id })
                        .then(function (add) {
                        var address = '';
                        address = " " + add[0].address + " " + add[0].city + "\n" + add[0].state + " " + add[0].country;
                        totalcost_1 = totalcost_1 + cartdetails_1[k].product_cost * cartdetails_1[k].count;
                        var newData = new order_1.default({
                            product_id: cartdetails_1[k]._id,
                            customer_id: req.body._id,
                            count: cartdetails_1[k].count,
                            total_cost: result[0].product_cost * cartdetails_1[k].count,
                            order_id: orderId_1,
                            address: address
                        });
                        newData
                            .save()
                            .then(function (result) {
                            cart_1.default.deleteOne({ customer_id: req.body._id, product_id: cartdetails_1[k]._id })
                                .then(function (result) {
                                // res.status(200).json({ status_code: 200, success: true, message: "order details successfully inserted in collection", cart: result });
                            });
                        });
                    });
                });
            };
            for (var k = 0; k < bodylength_1; k++) {
                _loop_1(k);
            }
            setTimeout(function () {
                totalcost_1 = totalcost_1 + (totalcost_1 * 0.05);
                order_1.default.updateMany({ customer_id: req.body._id, order_id: orderId_1 }, {
                    cart_cost: totalcost_1
                })
                    .then(function (res) {
                    console.log(res);
                });
            }, 2000);
            res.status(200).json({ status_code: 200, success: true, message: "order details successfully inserted in collection" });
        }
        // for logout (details details)
        else {
            var _loop_2 = function (l) {
                // for new product logout
                cart_1.default.find({ customer_id: req.body._id, product_id: cartdetails_1[l].product_id })
                    .then(function (data) {
                    if (data.length == 0) {
                        var newData = new cart_1.default({
                            product_id: cartdetails_1[l]._id,
                            customer_id: req.body._id,
                            count: cartdetails_1[l].count
                        });
                        newData.save();
                    }
                    // for existing product logout
                    else {
                        cart_1.default.update({ product_id: cartdetails_1[l].product_id, customer_id: req.body._id }, {
                            count: cartdetails_1[l].count
                        })
                            .then(function (result) {
                            console.log("result");
                            // res.status(200).json({ status_code: 200, success: true, message: "Cart details successfully inserted in collection", cart: result });
                        });
                    }
                })
                    .catch(function (err) {
                    res
                        .status(404)
                        .json({
                        status_code: 404,
                        success: false,
                        message: "Something went wrong",
                        error_message: err
                    });
                });
            };
            for (var l = 0; l < bodylength_1; l++) {
                _loop_2(l);
            }
            res.status(200).json({ status_code: 200, success: true, message: "Cart details successfully inserted in collection" });
        }
    }
    catch (err) {
        res
            .status(404)
            .json({
            status_code: 404,
            success: false,
            message: "Something went wrong",
            error_message: err
        });
    }
};
exports.default = newCart;
