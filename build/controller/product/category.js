"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// for inserting data in category collections
var category_1 = __importDefault(require("../../Models/product/category"));
/**
* @param category_name
*/
var newCategory = function (req, res) {
    var newData = new category_1.default(req.body);
    newData
        .save()
        .then(function (result) {
        res.status(200).json({ status_code: 200, success: true, message: "Product category details successfully inserted", customer: result });
    })
        .catch(function (err) {
        res
            .status(404)
            .json({
            status_code: 404,
            success: false,
            message: "Something went wrong",
            error_message: err
        });
    });
};
exports.default = newCategory;
