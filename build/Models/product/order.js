"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = __importDefault(require("mongoose"));
var Schema = mongoose_1.default.Schema;
var Order = new Schema({
    product_id: {
        type: String,
        ref: "Product"
    },
    customer_id: {
        type: String,
        ref: "Customer"
    },
    count: {
        type: Number,
        required: true
    },
    flag: {
        type: Boolean,
        required: true,
        default: false
    },
    order_id: {
        type: String,
        required: true
    },
    total_cost: {
        type: Number,
        required: true,
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    cart_cost: {
        type: Number,
        default: 0
    },
    address: {
        type: String
    }
});
var OrderModel = mongoose_1.default.model('Order', Order);
exports.default = OrderModel;
