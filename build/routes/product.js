"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var product_1 = __importDefault(require("../controller/product/product"));
var mutler_1 = __importDefault(require("../config/mutler"));
var allProduct_1 = __importDefault(require("../controller/product/allProduct"));
var productByCat_1 = __importDefault(require("../controller/product/productByCat"));
var category_1 = __importDefault(require("../controller/product/category"));
var color_1 = __importDefault(require("../controller/product/color"));
var productById_1 = __importDefault(require("../controller/product/productById"));
var productByColor_1 = __importDefault(require("../controller/product/productByColor"));
var productByBoth_1 = __importDefault(require("../controller/product/productByBoth"));
var saveCart_1 = __importDefault(require("../controller/product/saveCart"));
var showCart_1 = __importDefault(require("../controller/product/showCart"));
var token_1 = __importDefault(require("../config/token"));
var showColor_1 = __importDefault(require("../controller/product/showColor"));
var showCategory_1 = __importDefault(require("../controller/product/showCategory"));
var topRated_1 = __importDefault(require("../controller/product/topRated"));
var updateRating_1 = __importDefault(require("../controller/product/updateRating"));
var order_1 = __importDefault(require("../controller/product/order"));
var deleteCart_1 = __importDefault(require("../controller/product/deleteCart"));
var invoice_1 = __importDefault(require("../controller/product/invoice"));
var shippedcount_1 = __importDefault(require("../controller/product/shippedcount"));
var Product = /** @class */ (function () {
    function Product() {
    }
    Product.prototype.routes = function (app) {
        app.route('/product')
            .post(mutler_1.default.array("product_image"), product_1.default);
        app.route('/allProduct')
            .get(allProduct_1.default);
        app.route('/productByCat')
            .post(productByCat_1.default);
        app.route('/productById')
            .post(productById_1.default);
        app.route('/productByColor')
            .post(productByColor_1.default);
        app.route('/newCategory')
            .post(category_1.default);
        app.route('/productByBoth')
            .get(productByBoth_1.default);
        app.route('/newColor')
            .post(color_1.default);
        app.route('/newCart')
            .post(token_1.default, saveCart_1.default);
        app.route('/cartDetails')
            .get(token_1.default, showCart_1.default);
        app.route('/allcolor')
            .get(showColor_1.default);
        app.route('/allCategory')
            .get(showCategory_1.default);
        app.route('/topRated')
            .get(topRated_1.default);
        app.route('/updaterating')
            .put(token_1.default, updateRating_1.default);
        app.route('/order')
            .get(token_1.default, order_1.default);
        app.route('/deletecart/:product_id')
            .delete(token_1.default, deleteCart_1.default);
        app.route('/shippedcount')
            .get(token_1.default, shippedcount_1.default);
        app.route('/invoice')
            .post(token_1.default, invoice_1.default);
    };
    return Product;
}());
exports.Product = Product;
