"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var customer_1 = __importDefault(require("../controller/customer/customer"));
var login_1 = __importDefault(require("../controller/customer/login"));
var updateProfile_1 = __importDefault(require("../controller/customer/updateProfile"));
var token_1 = __importDefault(require("../config/token"));
var address_1 = __importDefault(require("../controller/customer/address"));
var mutler_1 = __importDefault(require("../config/mutler"));
var updateAddress_1 = __importDefault(require("../controller/customer/updateAddress"));
var showAddress_1 = __importDefault(require("../controller/customer/showAddress"));
var deleteAddress_1 = __importDefault(require("../controller/customer/deleteAddress"));
var verify_1 = __importDefault(require("../controller/customer/verify"));
var delivery_1 = __importDefault(require("../controller/customer/delivery"));
var Customer = /** @class */ (function () {
    function Customer() {
    }
    Customer.prototype.routes = function (app) {
        app.route('/customerRegis')
            .post(customer_1.default);
        app.route('/login')
            .post(login_1.default);
        // app.route('/profile')
        // .put(verifyToken,updatePro)
        app.route('/profile')
            .put(mutler_1.default.single("profile_image"), token_1.default, updateProfile_1.default);
        app.route('/address')
            .post(token_1.default, address_1.default);
        app.route('/updateaddress')
            .put(token_1.default, updateAddress_1.default);
        app.route('/showAddress')
            .get(token_1.default, showAddress_1.default);
        app.route('/deleteaddress/:address_id')
            .delete(token_1.default, deleteAddress_1.default);
        app.route('/verify')
            .get(token_1.default, verify_1.default);
        app.route('/delivery/:address_id')
            .put(token_1.default, delivery_1.default);
    };
    return Customer;
}());
exports.Customer = Customer;
