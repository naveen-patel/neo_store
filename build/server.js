"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var app_1 = __importDefault(require("./app"));
var mongoose_1 = __importDefault(require("mongoose"));
var PORT = 2300;
app_1.default.listen(PORT, function () {
    console.log("Server is running");
    mongoose_1.default.connect('mongodb://localhost:27017/neo', { useNewUrlParser: true })
        .then(function () { return console.log("Database Connected"); })
        .catch(function (err) { return console.log("Error is", err); });
});
