import app from './app'
import mongoose from 'mongoose'
const PORT = 2300

app.listen(PORT, () => {
    console.log("Server is running")
    mongoose.connect('mongodb://localhost:27017/neo', { useNewUrlParser: true })
        .then(() => console.log("Database Connected"))
        .catch(err => console.log("Error is", err))
})
