import mongoose from 'mongoose'
const Schema = mongoose.Schema

const Customer = new Schema({
    first_name: {
        type: String,
        required: true,
    },
    last_name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true,

    },
    gender: {
        type: String,
        required: true
    },
    phone: {
        type: Number,
        required: true
    },
    dob: {
        type: String,
    },
    profile_image: {
        type: String,

    }
})

const CustomerModel = mongoose.model('Customer', Customer)

export default CustomerModel