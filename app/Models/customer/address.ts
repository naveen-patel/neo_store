import mongoose from 'mongoose'
const Schema = mongoose.Schema

const Address = new Schema({
    customer_id: {
        type: String,
        required: true,
    },
    address: {
        type: String,
        required: true,
    },
    pincode: {
        type: Number,
        required: true,
    },
    city: {
        type: String,
        required: true,
    },
    state: {
        type: String,
        required: true,

    },
    country: {
        type: String,
        required: true
    },
    flag: {
        type: Boolean,
        required: true,
        default: false
    }
})

const AddressModel = mongoose.model('Address', Address)

export default AddressModel