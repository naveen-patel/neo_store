import mongoose from 'mongoose'
const Schema = mongoose.Schema

const rating = new Schema({
    customer_id: {
        type: String,
        required: true,
    },
    product_id: {
        type: String,
        required: true,
    },
    rating: {
        type: Number,
        required: true,
        default: false
    }
})

const RatingModel = mongoose.model('rating', rating)

export default RatingModel