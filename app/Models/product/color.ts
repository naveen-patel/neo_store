import mongoose from 'mongoose'
const Schema = mongoose.Schema

const ProductColorSchema = new Schema({
    color_name: {
        type: String,
        required: true,
    },
    color_code: {
        type: String,
        required: true
    }
})


const ProductColor = mongoose.model('Color', ProductColorSchema)

export default ProductColor