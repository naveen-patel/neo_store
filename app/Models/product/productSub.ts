import * as mongoose from 'mongoose'
import { fileURLToPath } from 'url';
const Schema = mongoose.Schema

//Mongoose Schema for Product Category
const ProductSubImages = new Schema({
    product_subImages: {
        type: [{ type: String }],
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now,
        required: true
    },
    product_image: {
        type: fileURLToPath,
        required: true
    }
})

//Product Category Model
const ProductSubImagesModel = mongoose.model('ProductSubImages', ProductSubImages)

//Available for controller
export default ProductSubImagesModel;