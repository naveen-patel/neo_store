import mongoose from 'mongoose'
const Schema = mongoose.Schema

const Product = new Schema({
    category_id: {
        type: String,
        ref: "Category"
    },
    color_id: {
        type: String,
        ref: "Color"
    },
    product_name: {
        type: String,
        required: true
    },
    product_image: {
        type: Array,
        required: true
    },
    product_desc: {
        type: String,
        required: true
    },
    product_rating: {
        type: Number,
        required: true
    },
    product_producer: {
        type: String,
        required: true
    },
    product_cost: {
        type: Number,
        required: true
    },
    product_stock: {
        type: Number,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    product_dimensions: {
        type: String,
        required: true
    },
    product_material: {
        type: String,
        required: true
    },
    product_type: {
        type: String,
        required: true
    },
    product_ratingCount: {
        type: String,
        required: true,
        default: 1
    },
    totalrating:{
        type:Number,
        required:true,
        default:0
    }
})

const ProductModel = mongoose.model('Product', Product)

export default ProductModel