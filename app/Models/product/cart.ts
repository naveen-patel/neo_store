import mongoose from 'mongoose'
import { string, required } from 'joi';
const Schema = mongoose.Schema

const Cart = new Schema({
    product_id: {
        type: String,
        ref: "Product",
        required:true
    },
    customer_id: {
        type: String,
        ref: "Customer",
        required:true
    },
    count: {
        type: Number,
        required: true
    },
    flag: {
        type: Boolean,
        required: true,
        default: true
    }
})

const CartModel = mongoose.model('Cart', Cart)

export default CartModel