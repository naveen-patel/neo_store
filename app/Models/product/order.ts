import mongoose from 'mongoose'
import { string } from 'joi';
const Schema = mongoose.Schema

const Order = new Schema({
    product_id: {
        type: String,
        ref: "Product"
    },
    customer_id: {
        type: String,
        ref: "Customer"
    },
    count: {
        type: Number,
        required: true
    },
    flag: {
        type: Boolean,
        required: true,
        default: false
    },
    order_id: {
        type: String,
        required: true
    },
    total_cost: {
        type: Number,
        required: true,
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    cart_cost:{
        type: Number,
        default:0
    },
    address:{
        type: String
    }

})

const OrderModel = mongoose.model('Order', Order)

export default OrderModel