import mongoose from 'mongoose'
const Schema = mongoose.Schema

const ProductCategorySchema = new Schema({
    category_name: {
        type: String,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now,
        required: true
    }

})

const ProductCategory = mongoose.model('Category', ProductCategorySchema)

export default ProductCategory