import jwt from 'jsonwebtoken'
const verifyToken = function (req: any, res: any, next: any) {
    const bearerHeader = req.headers['authorization'];
    const token = bearerHeader;
    req.token = token;
    jwt.verify(token, 'secretkey', function (err: any, authData: any) {
        if (err) {
            return res.status(404).json({ success: false, message: "Please login first !!!!", error_message: err })
        }
        else {
            req.body._id = authData.id;
            next();
        }
    });
}
export default verifyToken