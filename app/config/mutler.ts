import multer from 'multer'
const storage = multer.diskStorage({
    destination: function (request, file, cb) {
      cb(null, './upload/')
    },
    filename: function (request, file, cb) {
      cb(null, new Date().toISOString().replace(/:/g, '-') + file.originalname);
    }
  })
  let upload = multer({ storage: storage });

  export default upload;