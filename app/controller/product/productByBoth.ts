// for find product by both color and category
import ProductModel from "../../Models/product/product";
import { Request, Response } from "express";

/**
* @param category_id to sort products according to category id
* @param color_id to sort products according to color id
* @param sortBy to sort product according to rating or price(sortBy=product_rating)
* @param desc shows the way of sorting product (sortBy=product_price&desc=false) will sort product according to price in descending order 
* @param text for searching product
*/
const productByBoth = (req: Request, res: Response) => {

    const a: any = {}
    const { sort = '{}', limit, page,text='', sortBy, desc, ...others } = req.query; // destructering
    if(text.trim()){
        a.product_name={$regex:"^"+text,$options:'i'}
        }
    Object.entries(others).forEach(item => { if (item[1]) a[item[0]] = item[1]; })
    ProductModel.find(a).sort(sortBy ? { [sortBy]: desc == "true" ? -1 : 1 } : {})
    .limit(parseInt(req.query.limit)).skip(parseInt(req.query.limit)*(parseInt(req.query.page)-1))
        .populate('category_id') 
        .populate('color_id')
        .then(result => {
            ProductModel.find(a).sort(sortBy ? { [sortBy]: desc == "true" ? -1 : 1 } : {})
            .then(data=>{
                // if(result.length==0)
                // {
                //     res.status(200).json({ success: 'true', status: 200, message:"Product not found!!!" })
                
                // }
                // else{

                    res.status(200).json({ success: 'true', status: 200, product: result, count: result.length, totalcount:data.length })
                // }

            })
        })
        .catch(err => {
            res.status(404).json({ success: 'false', status: 404, err: err.details });
        })
};

export default productByBoth;