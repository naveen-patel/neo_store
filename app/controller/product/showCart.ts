// for cart details (return all product from cart)
import CartModel from "../../Models/product/cart"
import { Request, Response } from "express";
import ProductModel from "../../Models/product/product";
/**
* @param _id user id from token whose address to be deleted
* @returns all product of cart
*/
const cartDetails = (req: Request, res: Response) => {
  
    CartModel.find({ customer_id: req.body._id})
        .populate('product_id')
        .then(result => {
            res.status(200).json({
                status_code: 200, success: true, message: "cart details", data: result})
        })
        .catch(err => {
            res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err })
        })
}

export default cartDetails 