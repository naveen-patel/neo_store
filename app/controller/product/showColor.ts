// for all color and color code (return color with code)
import ProductColor from "../../Models/product/color"
import { Request, Response } from "express";

const allColor = (req: Request, res: Response) => {
    ProductColor.find()

        .then(result => {
            res.status(200).json({ status_code: 200, success: true, message: "All color and color code of product", product: result })
        })
        .catch(err => {
            res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err })
        })
}

export default allColor