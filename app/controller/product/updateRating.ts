// for user address  details update

import ProductModel from "../../Models/product/product"
import { Request, Response } from 'express'
import RatingModel from "../../Models/customer/rating"
import { finished } from "stream";
/**
* @param _id id of product whose rating needs to be updated
* @param rating new rating given by user
* @returns new average rating 
*/

const updaterating = (req: Request, res: Response) => {
    if (req.body.product_rating <= 5 && req.body.product_rating >= 0) {
        RatingModel.find({ product_id: req.body.product_id, customer_id: req.body._id })
            .then(data => {
                if (data.length == 0) {
                    ProductModel.find({ _id: req.body.product_id })
                        .then(result => {
                            let total = (parseFloat(result[0].totalrating) + parseFloat(req.body.product_rating));
                            let count = (parseFloat(result[0].product_ratingCount) + 1)
                            let average = total / count;
                            ProductModel.findByIdAndUpdate(req.body.product_id, {
                                product_rating: average,
                                product_ratingCount: count,
                                totalrating: total
                            })
                                .then(result => {
                                    ProductModel.find({ _id: req.body.product_id })
                                        .then(result => {
                                            let newData = new RatingModel({
                                                customer_id: req.body._id,
                                                product_id: req.body.product_id,
                                                rating: req.body.product_rating
                                            });
                                            newData
                                                .save()
                                                .then(result => {

                                                    res.status(200).json({ status_code: 200, success: true, message: "Product rating successfully updated ", details: result })
                                                })
                                        })
                                })

                        })

                }
                else {
                    res.status(404).json({ status_code: 404, success: false, message: "Sorry you already rate it" })
                }
            })
            .catch(err => {
                res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err })
            })
    }
    else {
        res.status(404).json({ status_code: 404, success: false, message: "rating not correct (0<=5)" })

    }
}

export default updaterating