// for all color and color code (return color with code)
import ProductCategory from "../../Models/product/category"
import { Request, Response } from "express";

const allCategory = (req: Request, res: Response) => {
    ProductCategory.find()

        .then(result => {
            res.status(200).json({ status_code: 200, success: true, message: "All category of product", product: result })
        })
        .catch(err => {
            res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err })
        })
}

export default allCategory