// for inserting data in cart collections
import CartModel from "../../Models/product/cart"
import OrderModel from "../../Models/product/order"
import { Request, Response } from "express";
import { date } from "joi";
import ProductModel from "../../Models/product/product";
import AddressModel from "../../Models/customer/address";

const newCart = (req: Request, res: Response) => {
  try {

    // for unique orderId
    let orderId = "order_id" + Date.now()

    // for checkout (order details save )
    let cartdetails = req.body;
    let bodylength = cartdetails.length -1
    console.log('cart details ', req.body)
    console.log('bodylength ', cartdetails.length)
    console.log('logout', cartdetails[bodylength].flag)
    console.log('checkout', cartdetails[bodylength-1].flag)
    if (cartdetails[bodylength].flag == true || cartdetails[bodylength-1].flag == true) {
      console.log('if for checkout')
      let totalcost = 0;
      for (let k = 0; k < bodylength-1; k++) {
        

        ProductModel.find({ _id: cartdetails[k]._id })
          .then(result => {
            AddressModel.find({_id:cartdetails[bodylength-1].address_id})
            .then(add=>{
              let address:string = ''
                  address=" "+add[0].address + " " + add[0].city + "\n" + add[0].state +" "+add[0].country
              totalcost = totalcost + cartdetails[k].product_cost * cartdetails[k].count
            let newData = new OrderModel({
              product_id: cartdetails[k]._id,
              customer_id: req.body._id,
              count: cartdetails[k].count,
              total_cost: result[0].product_cost * cartdetails[k].count,
              order_id: orderId,
              address:address
            });
            newData
              .save()
              .then(result => {
                console.log('checkout')
                CartModel.deleteOne({ customer_id:req.body._id,product_id: cartdetails[k]._id })
                  .then(result => {
                    // res.status(200).json({ status_code: 200, success: true, message: "order details successfully inserted in collection", cart: result });
                  })
              })


            })
            

            
          })
      }
      setTimeout(() => {
        totalcost = totalcost + (totalcost * 0.05)
        OrderModel.updateMany({ customer_id: req.body._id, order_id: orderId }, {
          cart_cost: totalcost
        })
          .then(res => {
            console.log(res)
          })
      }, 2000)

      res.status(200).json({ status_code: 200, success: true, message: "order details successfully inserted in collection" });

    }

    // for logout (details details)
    else {
      console.log('else',bodylength)
      for (let l = 0; l < bodylength; l++) {
        console.log('for')
        // for new product logout

        CartModel.find({ customer_id: req.body._id, product_id: cartdetails[l].product_id })
          .then(data => {
            if (data.length == 0) {
              console.log('for new')
              let newData = new CartModel({
                product_id: cartdetails[l]._id,
                customer_id: req.body._id,
                count: cartdetails[l].count
              });
              newData.save()
              console.log('logout add product')
            }
            // for existing product logout
            else {
              CartModel.update({product_id:cartdetails[l].product_id,customer_id:req.body._id}, {
                count: cartdetails[l].count
              })
                .then(result => {
                  console.log("result")
                  console.log('update prodcut')
                  // res.status(200).json({ status_code: 200, success: true, message: "Cart details successfully inserted in collection", cart: result });
                })
            }
          })
          .catch(err => {
            res
              .status(404)
              .json({
                status_code: 404,
                success: false,
                message: "Something went wrong",
                error_message: err
              });
          });
      }
      res.status(200).json({ status_code: 200, success: true, message: "Cart details successfully inserted in collection" });

    }
  }
  catch (err) {
    res
      .status(404)
      .json({
        status_code: 404,
        success: false,
        message: "Something went wrong",
        error_message: err
      });
  }
}
export default newCart