// for find product by category id
import ProductModel from "../../Models/product/product"
import { Request, Response } from "express";

const productByCat = (req: Request, res: Response) => {

    ProductModel.find({ category_id: req.body.cat_id, })
        .populate('category_id')
        .populate('color_id')
        .then(result => {
            res.status(200).json({ status_code: 200, success: true, message: "success", product: result })
        })
        .catch(err => {
            res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err })
        })
}

export default productByCat