import { Request, Response } from 'express'
import fs from 'fs'
import PDFDocument from 'pdfkit'
import CustomerModel from '../../Models/customer/customer'
import moment from 'moment'
import AddressModel from '../../Models/customer/address';
const invoice = async (req: Request, res: Response) => {

    let orderData = req.body
//    let add=await AddressModel.find({_id:orderData.products[0].address})
   let address:string = ''

//     address=" "+add[0].address + " " + add[0].city + "\n" + add[0].state +" "+add[0].country
    let name: string = ''
    let phone: number = 0
    let table_y: any = []
    let yValue: number
    let data = await CustomerModel.find({_id:req.body._id})
    name = data[0].first_name + " " + data[0].last_name
    phone = data[0].phone
    address = orderData.products[0].address
    let filename = orderData._id

    let d = new Date();

    filename = "NeoSTORE" + "_" + filename + "_" + "(" + d.toDateString() + ")" + '.pdf'

    let doc = new PDFDocument({ margin: 50 })

    generateHeader(doc);
    generateCustomerInformation(doc, orderData, name, phone,address);
    generateInvoiceTable(doc, orderData, table_y);
    yValue = table_y[table_y.length - 1]
    generateFooter(doc, yValue);

    doc.pipe(fs.createWriteStream('invoice/' + filename));

    doc.end();
    res.status(200).json({ success: true, status_code: 200, message: `Your Invoice of Order No ${orderData._id} has been generated`, receipt: filename })
}

const generateHeader = (doc) => {

    doc
        .image("upload/logo.png", 50, 40, { width: 100, height: 50 })
        .fillColor("#444444")
        .fontSize(10)
        .text("5th Floor, SIGMA IT PARK", 200, 65, { align: "right" })
        .text("Rabale, MUMBAI, INDIA, 400701", 200, 80, { align: "right" })
        .moveDown();
}

const generateFooter = (doc, yValue) => {
    let footer_y: any = []

    for (let i = 0; i < 4; i++) {
        yValue = yValue + 20
        footer_y.push(yValue)
    }

    doc
        .fontSize(12)
        .text("Shipping will be done within a week.", 50, footer_y[0], { align: "center" })
        .text("If you have any question about this invoice, please contact", 50, footer_y[1], { align: "center" })
        .fontSize(11)
        .font('Helvetica-Bold')
        .text("NeoSOFT Technologies, +91 1234567891, contact@neosofttech.com", 50, footer_y[2], { align: "center" })
        .fillColor('Blue')
        .text("Thank You For Your Buisness", 50, footer_y[3], { align: "center" })

    footer_y[footer_y.length - 1] = footer_y[footer_y.length - 1] + 20

    doc
        .lineCap('butt')
        .moveTo(50, footer_y[footer_y.length - 1])
        .lineTo(560, footer_y[footer_y.length - 1])
        .stroke()

    footer_y[footer_y.length - 1] = footer_y[footer_y.length - 1] + 20

    doc
        .text("Invoice Template by NeoSOFT Technologies", 50, footer_y[footer_y.length - 1])
        .image("upload/logo.jpg", 450, footer_y[footer_y.length - 1], { width: 80, height: 40, align: "right" })
}

const generateCustomerInformation = (doc, orderData, name, phone,address) => {
    let date= moment(orderData.products[0].created_at).format('DD/MM/YYYY')

    doc
        .fontSize(15)
        .text(`Invoice Details`, 50, 120)
        .lineCap('butt')
        .moveTo(50, 140)
        .lineTo(560, 140)
        .stroke()
        .fontSize(12)
        .text(`ORDER Number: ${orderData.products[0].order_id}`, 50, 160)
        .text(`ORDER Date: ${date}`, 50, 180)
        .text(`Total Cost: ${orderData.products[0].cart_cost}`, 50, 200)
        .fontSize(12)
        .text(`Name: ${name}`, 380, 160)
        .text(`Mobile No: ${phone}`, 380, 180)
        .text(`Shipping Address:${address}`, 380, 200)
        .fontSize(11)
        //.text(`${orderData.deliveryAddress[0].address}, ${orderData.deliveryAddress[0].pincode}, ${orderData.deliveryAddress[0].city}, ${orderData.deliveryAddress[0].state}, ${orderData.deliveryAddress[0].country}`, 380, 220)
        .lineCap('butt')
        .moveTo(50, 245)
        .lineTo(560, 245)
        .stroke()
        .moveDown();

}

const generateTableRow = (doc, y, c1, c2, c3, c4, c5) => {
    doc
        .fontSize(11)
        .text(c1, 60, y)
        .text(c2, 100, y)
        .text(c3, 280, y, { width: 90, })
        .text(c4, 380, y, { width: 90, })
        .text(c5, 470, y, { width: 90, })

}

const generateInvoiceTable = (doc, orderData, table_y) => {
    let i
    let invoiceTableTop:number=340
    let sum:number=0
    let position
    let gst

    doc
        .fontSize(15)
        .text(`Product Details`, 50, 280)
        .lineCap('butt')
        .moveTo(50, 300)
        .lineTo(560, 300)
        .stroke()
        .fontSize(13)
        .text(`SNO`, 50, 320)
        .text(`Name`, 130, 320)
        .text(`Price`, 280, 320)
        .text(`Quantity`, 360, 320)
        .text(`Total Price`, 450, 320)
        .lineCap('butt')
        .moveTo(50, 340)
        .lineTo(560, 340)
        .stroke()

    for (i = 0; i < orderData.products.length; i++) {
        const product = orderData.products[i];
        position = invoiceTableTop + (i + 1) * 20;
        let count = i + 1
        sum = sum + parseInt(product.total_cost)

        generateTableRow(
            doc,
            position,
            count,
            product.order[0].product_name,
            product.order[0].product_cost,
            product.count,
            product.total_cost,
        );
    }

    position = position + 20
    doc
        .lineCap('butt')
        .moveTo(50, position)
        .lineTo(560, position)
        .stroke()

    for (let j = 0; j < 3; j++) {
        position = position + 20;
        table_y.push(position)
    }

    gst = Math.round((5 * sum) / 100)

    doc
        .fontSize(13)
        .text(`Total`, 360, table_y[0])
        .text(`${sum}`, 470, table_y[0])
        .text(`GST (5%)`, 360, table_y[1])
        .text(`${gst}`, 470, table_y[1])
        .text(`Net Total.`, 360, table_y[2])
        .text(`${orderData.products[0].cart_cost}`, 470, table_y[2])

    table_y[table_y.length - 1] = table_y[table_y.length - 1] + 20

    doc
        .lineCap('butt')
        .moveTo(50, table_y[table_y.length - 1])
        .lineTo(560, table_y[table_y.length - 1])
        .stroke()

}

export default invoice