// for all product (return all product)
import ProductModel from "../../Models/product/product"
import { Request, Response } from "express";
/**
* @returns all product details
*/
const allProduct = (req: Request, res: Response) => {
    ProductModel.find().select("product_name")
    .populate('category_id')
    
                .then(result => {
                    res.status(200).json({ status_code:200,success: true, message: "all product", product:result})
                })
                .catch(err => {
                    res.status(404).json({ status_code:404,success: false, message: "Something went wrong", error_message: err })
                })
    }

    export default allProduct