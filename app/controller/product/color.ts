// for inserting data in color collections

import ProductColor from "../../Models/product/color"
import { Request, Response } from "express";

const newColor = (req: Request, res: Response) => {
/**
* @param color_name 
* @param color_code address id
*/
    let newData = new ProductColor(req.body);
      newData
        .save()
        .then(result => {
          res.status(200).json({status_code:200,success: true,message: "Product color details successfully inserted",customer: result});
        })
   .catch(err => {
      res
        .status(404)
        .json({
          status_code:404,
          success: false,
          message: "Something went wrong",
          error_message: err
        });
    });
  
}  

export default newColor