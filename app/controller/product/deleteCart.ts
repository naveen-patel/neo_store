// for delete user address details

import CartModel from '../../Models/product/cart'
import { Request, Response } from 'express'

/**
* @param _id user id from token whose address to be deleted
* @param cart_Id cart id
* @returns delete selected product form cart
*/
const deletecart = (req: Request, res: Response) => {
    CartModel.deleteOne({product_id:req.params.product_id,customer_id:req.body._id}) 
                .then(result => {       
                    
                        res.status(200).json({ status_code:200,success: true, message: "product successfully deleted ",details:result})
                    })
                .catch(err => {
                    res.status(404).json({ status_code:404,success: false, message: "Something went wrong", error_message: err })
                })
    }   
 
    export default deletecart