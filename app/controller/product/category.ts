// for inserting data in category collections
import ProductCategory from "../../Models/product/category"
import { Request, Response } from "express";
/**
* @param category_name
*/
const newCategory = (req: Request, res: Response) => {

    let newData = new ProductCategory(req.body);
      newData
        .save()
        .then(result => {
          res.status(200).json({status_code:200,success: true,message: "Product category details successfully inserted",customer: result});
        })
   .catch(err => {
      res
        .status(404)
        .json({
          status_code:404,
          success: false,
          message: "Something went wrong",
          error_message: err
        });
    });
  
}  

export default newCategory