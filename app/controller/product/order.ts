//for order details of 

import OrderModel from "../../Models/product/order"
import { Request, Response } from "express";
/**
* @param _id user id from token whose address to be deleted
* @returns group of cart product by orderId
*/
const order = (req: Request, res: Response) => {
    OrderModel.aggregate([
        ([
            { $match: { customer_id: req.body._id} },
            {

                $lookup:
                {
                    from: "products",
                    localField: "product_id",
                    foreignField: "product_id",
                    as: "order"
                }
            },
            {
                $group:
                {
                    _id: "$order_id",
                    products: { $push: "$$ROOT" }
                    //products: { $first: "$$ROOT" }
                }
            }

        ])
    ])


        .then(result => {
            res.status(200).json({ status_code: 200, success: true, message: "order details of customer", product: result })
        })
        .catch(err => {
            res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err })
        })
}


export default order

