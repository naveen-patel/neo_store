import ProductModel from "../../Models/product/product"
import { Request, Response } from "express";
import OrderModel from "../../Models/product/order";
/**
* @param _id user id from token whose address to be deleted
* @returns all address of customers
*/
const topRated = (req: Request, res: Response) => {
    ProductModel.aggregate([
        ([{
            $lookup:
            {
                from: "colors",
                localField: "color_id",
                foreignField: "color_id",
                as: "color_id"
            }
        },
        {
            $sort:
                { product_rating: -1 }
        },
        {
            $group:
            {
                _id: "$category_id",
                // products:{$push:"$$ROOT"}
                products: { $first: "$$ROOT" }
            }
        }
        ])
    ])


        .then(result => {
                res.status(200).json({ status_code: 200, success: true, message: "Top rated product", product: result })
            
        })
        .catch(err => {
            res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err })
        })
}


export default topRated

