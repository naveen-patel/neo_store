//for product by id (return one product's details with color and category )

import ProductModel from "../../Models/product/product"
import { Request, Response } from "express";

const productById = (req: Request, res: Response) => {
    // const { email, prod_color="red", prod_type } = req.body;

    ProductModel.find({_id:req.body._id})
    .populate('color_id')
    .populate('category_id')
                .then(result => {
                    res.status(200).json({status_code:200, success: true, message: "product detail by id", product:result})
                })
                .catch(err => {
                    res.status(404).json({ status_code:404,success: false, message: "Something went wrong", error_message: err })
                })
    }

    export default productById