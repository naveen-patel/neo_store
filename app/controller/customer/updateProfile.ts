// for user profile details update

import CustomerModel from '../../Models/customer/customer'
import { Request, Response } from 'express'
import Joi from "joi";
import jwt from 'jsonwebtoken';


/**
* @param first_name
* @param last_name
* @param gender
* @param dob
* @param phone
* @param email
* @param profile_image
* @returns update profile of user
*/
const updatePro = (req: Request, res: Response) => {
    CustomerModel.findByIdAndUpdate(req.body._id, {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        gender: req.body.gender,
        dob: req.body.dob,
        phone: req.body.phone,
        email: req.body.email,
        profile_image: req.file.filename
    })

        .then(result => {
            CustomerModel.find({ _id: req.body._id })
                .then(result => {
                    let data=result
                    res.status(200).json({ status_code: 200, success: true, message: "user profile successfully update", details: data[0] })
                })

        })
        .catch(err => {
            res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err })
        })
}

export default updatePro