// for add address details
import AddressModel from "../../Models/customer/address"
import { Request, Response } from "express";

/**
* 
* @param customer_id customer id from token
* @param address address of customer
* @param pincode pincode of area
* @param city city
* @param state state
* @param country country
* 
*/
const address = (req: Request, res: Response) => {

    let newData = new AddressModel({
        customer_id: req.body._id,
        address: req.body.address,
        pincode: req.body.pincode,
        city: req.body.city,
        state: req.body.state,
        country: req.body.country,
    })

    newData.save()
        .then(result => {

            res.status(200).json({ status_code: 200, success: true, message: "user address successfully added", product: result })
        })
        .catch(err => {
            res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err })
        })
}

export default address 