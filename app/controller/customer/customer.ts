//for registration new user

import CustomerModel from "../../Models/customer/customer";
import { Request, Response } from "express";
import Joi from "joi";
import bcrypt from "bcrypt";
import sendmail from 'sendmail'


const newCustomer = (req: Request, res: Response) => {

  const emailSender = sendmail({
    silent: false
  });

  const sendEmail = (options: sendmail.MailInput): Promise<boolean> =>
    new Promise((resolve, reject) => {
      emailSender(options, (err, reply) => {
        // if error happened or returned code is now started with 2**
        if (err || !reply.startsWith("2")) {
          reject(err);
        } else {
          resolve(true);
        }
      });
    });

  //for empty field
  if (req.body === undefined) {
    return res
      .status(404)
      .json({ success: false, message: "Something went wrong" });
  }
  // for validation
  const schema = Joi.object().keys({
    first_name: Joi.string().required(),
    last_name: Joi.string().required(),
    email: Joi.string()
      .email({ minDomainAtoms: 2 }).regex(/^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i)
      .required(),
    password: Joi.string()
      .regex(/^(?=.*[a-zA-Z])(?=.*[0-9])/)
      .required(),
    conpassword: Joi.any()
      .equal(Joi.ref("password"))
      .required(),
    gender: Joi.string().required(),
    phone: Joi.number()
      .integer()
      .required()
  });
  Joi.validate(req.body, schema, (err: any, value: string) => {
    if (err) {
      return res
        .status(400)
        .json({
          err,
          code: 400,
          message: "joi validation error",
          error_message: err
        });
    } else {
      // for password encryption
      const saltRounds = 10;
      const myPlaintextPassword = req.body.password;
      const salt = bcrypt.genSaltSync(saltRounds);
      const passwordHash = bcrypt.hashSync(myPlaintextPassword, salt);
      let output = `<div style="background-color:skyblue;"'>
     
<h3>Hi ${req.body.first_name+req.body.last_name} your details are mentioned below:</h3>
<ul>  
  <li>Name:</li>
  <li><a href="www.google.com">Visit here</a></li>
</ul>
</div>`

/**
* @param email
* @param password password must be minimum 4 characters long and 8 maximum characters long and should contains at least one capital letter, one small letter and one digit
* @param cnfpassword confirm password
* @param gender numeric in form of 0 or 1, 1 for male and 0 for female
* @param phone
* @param first_name
* @param last_name
*/
      let newData = new CustomerModel({
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email,
        password: passwordHash,
        gender: req.body.gender,
        phone: req.body.phone
      });
      newData
        .save()
        .then(result => {

          sendEmail({
            from: 'naveen.patel@neosofttech.com',
            to: req.body.email,
            subject: 'User login details',
            html: output,
          })
          res.status(200).json({ status_code: 200, success: true, message: "Customer Registration Successfully", customer: result });
        })
        .catch(err => {
          res
            .status(404)
            .json({
              success: false,
              status_code: 404,
              message: "Something went wrong",
              error_message: err
            });
        });

    }
  })
}
export default newCustomer;
