// for delete user address details

import AddressModel from '../../Models/customer/address'
import { Request, Response } from 'express'
/**
* @param _id user id from token whose address to be deleted
* @param address_id address id
* @returns delete selected address of user
*/
const deleteaddress = (req: Request, res: Response) => {
    
    AddressModel.deleteOne({_id:req.params.address_id,customer_id:req.body._id}) 
                .then(result => { 
                    
                        res.status(200).json({ status_code:200,success: true, message: "Address successfully deleted ",details:result})
                    })
                .catch(err => {
                    res.status(404).json({ status_code:404,success: false, message: "Something went wrong", error_message: err })
                })
    }   
 
    export default deleteaddress