// for user address  details update

import AddressModel from '../../Models/customer/address'
import { Request, Response } from 'express'

/**
* @param _id id of user from token

* @returns updates address of user
*/


const delivery = (req: Request, res: Response) => {
    AddressModel.findByIdAndUpdate(req.params.address_id, {
        flag:"true"
    })

        .then(result => {

            AddressModel.find({ _id:req.params.address_id })
                .then(result => {

                    res.status(200).json({ status_code: 200, success: true, message: "Address successfully updated ", details: result })
                })

        })
        .catch(err => {
            res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err })
        })
}

export default delivery