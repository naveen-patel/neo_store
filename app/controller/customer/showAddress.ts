// for show address (return all address of customer)
import AddressModel from '../../Models/customer/address'

import { Request, Response } from "express";
/**
* @param _id user id from token whose address to be deleted
* @returns all address of customers
*/
const showAddress = (req: Request, res: Response) => {
    AddressModel.find({ customer_id: req.body._id })
        .then(result => {
            res.status(200).json({ status_code: 200, success: true, message: "all address of customer ", product: result })
        })
        .catch(err => {
            res.status(404).json({ status_code: 404, success: false, message: "Something went wrong", error_message: err })
        })
}

export default showAddress