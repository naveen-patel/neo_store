import newProduct from '../controller/product/product'
import upload from '../config/mutler'
import allProduct from '../controller/product/allProduct';
import productByCat from '../controller/product/productByCat'
import newCategory from '../controller/product/category'
import newColor from '../controller/product/color';
import productById from '../controller/product/productById';
import productByColor from '../controller/product/productByColor'
import productByBoth from '../controller/product/productByBoth';
import newCart from '../controller/product/saveCart';
import cartDetails from '../controller/product/showCart';
import verifyToken from '../config/token';
import allcolor from '../controller/product/showColor'
import allCategory from '../controller/product/showCategory';
import topRated from '../controller/product/topRated';
import updaterating from '../controller/product/updateRating';
import order from '../controller/product/order';
import deletecart from '../controller/product/deleteCart';
import invoice from '../controller/product/invoice';
import shippedcount from '../controller/product/shippedcount';
import suggestion from '../controller/product/suggestion';
export class Product {
    public routes(app: any): void {
        app.route('/product')
            .post(upload.array("product_image"), newProduct)

        app.route('/allProduct')
            .get(allProduct)

        app.route('/productByCat')
            .post(productByCat)

        app.route('/productById')
            .post(productById)

        app.route('/productByColor')
            .post(productByColor)

        app.route('/newCategory')
            .post(newCategory)

        app.route('/productByBoth')
            .get(productByBoth)

        app.route('/newColor')
            .post(newColor)

        app.route('/newCart')
            .post(verifyToken,newCart)

        app.route('/cartDetails')
            .get(verifyToken, cartDetails)

        app.route('/allcolor')
            .get(allcolor)

        app.route('/allCategory')
            .get(allCategory)

        app.route('/topRated')
            .get(topRated)

        app.route('/updaterating')
            .put(verifyToken, updaterating)

        app.route('/order')
            .get(verifyToken, order)
            
        app.route('/deletecart/:product_id')
            .delete(verifyToken, deletecart)
         
            app.route('/shippedcount')
            .get(verifyToken, shippedcount)
            
            app.route('/invoice')
            .post(verifyToken, invoice)

            app.route('/suggestion')
            .get(suggestion)
    }

}