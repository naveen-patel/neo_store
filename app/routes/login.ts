import customerLogin from '../controller/customer/login'

export class Login{

    public routes(app:any):void{
        app.route('/login')
        .post(customerLogin)
    }
}