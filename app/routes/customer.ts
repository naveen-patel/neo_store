import newCustomer from '../controller/customer/customer'
import customerLogin from '../controller/customer/login'
import updatePro from '../controller/customer/updateProfile';
import verifyToken from '../config/token';
import address from '../controller/customer/address';
import upload from '../config/mutler'
import updateaddress from '../controller/customer/updateAddress';
import showAddress from '../controller/customer/showAddress';
import deleteaddress from '../controller/customer/deleteAddress';
import verify from '../controller/customer/verify';
import delivery from '../controller/customer/delivery';

export class Customer {

    public routes(app: any): void {
        app.route('/customerRegis')
            .post(newCustomer)

        app.route('/login')
            .post(customerLogin)

        // app.route('/profile')
        // .put(verifyToken,updatePro)

        app.route('/profile')
            .put(upload.single("profile_image"), verifyToken, updatePro)

        app.route('/address')
            .post(verifyToken, address)

        app.route('/updateaddress')
            .put(verifyToken, updateaddress)

        app.route('/showAddress')
            .get(verifyToken, showAddress)

        app.route('/deleteaddress/:address_id')
            .delete(verifyToken, deleteaddress)

             app.route('/verify')
            .get(verifyToken, verify)
            
            app.route('/delivery/:address_id')
            .put(verifyToken, delivery)

    }
}